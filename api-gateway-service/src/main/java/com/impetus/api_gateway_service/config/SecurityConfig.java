package com.impetus.api_gateway_service.config;


//import com.impetus.api_gateway_service.filters.AuthenticationFilter;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
//import org.springframework.security.config.web.server.ServerHttpSecurity;
//import org.springframework.security.web.server.SecurityWebFilterChain;
//import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatchers;
//
//
//@Configuration
//@EnableWebFluxSecurity
//public class SecurityConfig {
//
//    private final AuthenticationFilter authenticationFilter;
//
//    public SecurityConfig(AuthenticationFilter authenticationFilter) {
//        this.authenticationFilter = authenticationFilter;
//    }
//
//    @Bean
//    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
//        http.authorizeExchange(exchanges -> exchanges.anyExchange().authenticated())
//                .addFilterAt(authenticationFilter,
//                        org.springframework.security.web.server.SecurityWebFiltersOrder.AUTHENTICATION);
//
//        return http.build();
//    }
//}

import com.impetus.api_gateway_service.tokenAuthentication.AuthenticationToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    @Value("${security.token}")
    private String securityToken;

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http
                .csrf().disable()
                .authorizeExchange(exchanges -> exchanges
                        .pathMatchers("/api/inventory/**", "/api/payment/**","/user/**", "/registration/**","/order/**","/notification/**").authenticated()
                        .anyExchange().permitAll()
                )
                .securityContextRepository(securityContextRepository());

        return http.build();
    }

    @Bean
    public ServerSecurityContextRepository securityContextRepository() {
        return new ServerSecurityContextRepository() {
            @Override
            public Mono<Void> save(ServerWebExchange exchange, SecurityContext context) {
                return Mono.empty();
            }

            @Override
            public Mono<SecurityContext> load(ServerWebExchange exchange) {
                String authHeader = exchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
                if (authHeader != null && authHeader.equals("Bearer " + securityToken)) {
                    Authentication auth = new AuthenticationToken(securityToken);
                    SecurityContext securityContext = new SecurityContextImpl(auth);
                    return Mono.just(securityContext);
                }
                return Mono.empty();
            }
        };
    }
}

