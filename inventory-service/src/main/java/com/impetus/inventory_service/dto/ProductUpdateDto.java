package com.impetus.inventory_service.dto;

public class ProductUpdateDto {
    private Long oldProductId;
    private Long newProductId;

    private String productName;

    private int quantity;

    private double price;

    private String category;

    public ProductUpdateDto() {
    }

    public ProductUpdateDto(Long oldProductId, Long newProductId, String productName, int quantity, double price, String category) {
        this.oldProductId = oldProductId;
        this.newProductId = newProductId;
        this.productName = productName;
        this.quantity = quantity;
        this.price = price;
        this.category = category;
    }

    public Long getOldProductId() {
        return oldProductId;
    }

    public void setOldProductId(Long oldProductId) {
        this.oldProductId = oldProductId;
    }

    public Long getNewProductId() {
        return newProductId;
    }

    public void setNewProductId(Long newProductId) {
        this.newProductId = newProductId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
