package com.impetus.inventory_service.dto;

import lombok.Data;


public class ProductId {
    private Long productId;

    public ProductId() {
    }

    public ProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}
