package com.impetus.inventory_service.controller;
import com.impetus.inventory_service.dto.ProductUpdateDto;
import com.impetus.inventory_service.model.Inventory;
import com.impetus.inventory_service.service.InventoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/inventory")
public class InventoryController {

    private static final Logger logger = LoggerFactory.getLogger(InventoryController.class);

    @Autowired
    private InventoryService inventoryService;

    @GetMapping("fetchAllProductByProductId")
    public ResponseEntity<List<Inventory>> getAllInventories() {
        try {
            logger.debug("Fetching all inventories");
            List<Inventory> inventories = inventoryService.getAllInventories();
            logger.info("Successfully fetched all inventories");
            return new ResponseEntity<>(inventories, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error getting all inventories: {}", e.getMessage(), e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/fetchProductByProductId/{productId}")
    public ResponseEntity<Inventory> getInventoryById(@PathVariable("productId") Long productId) {
        try {
            logger.debug("Fetching inventory by id: {}", productId);
            Inventory inventory = inventoryService.getInventoryById(productId)
                    .orElseThrow(() -> new Exception("Inventory not found with id " + productId));
            logger.info("Successfully fetched inventory with id: {}", productId);
            return new ResponseEntity<>(inventory, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error getting inventory by id: {}. Error: {}", productId, e.getMessage(), e);
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("createProduct")
    public ResponseEntity<Inventory> createInventory(@RequestBody Inventory inventory) {
        try {
            logger.debug("Creating new inventory: {}", inventory);
            Inventory createdInventory = inventoryService.createInventory(inventory);
            logger.info("Successfully created inventory with id: {}", createdInventory.getProductId());
            return new ResponseEntity<>(createdInventory, HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Error creating inventory: {}", e.getMessage(), e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/updateProduct")
    public ResponseEntity<Inventory> updateInventory(@RequestBody ProductUpdateDto updateInventoryDetails) {
        try {
            logger.debug("Updating inventory with oldProductId: {} to newProductId: {}", updateInventoryDetails.getOldProductId(), updateInventoryDetails.getNewProductId());
            Inventory update = new Inventory(updateInventoryDetails.getNewProductId(), updateInventoryDetails.getProductName(),
                    updateInventoryDetails.getQuantity(), updateInventoryDetails.getPrice(), updateInventoryDetails.getCategory());
            Inventory updatedInventory = inventoryService.updateInventory(updateInventoryDetails.getOldProductId(), update);
            logger.info("Successfully updated inventory with oldProductId: {} to newProductId: {}", updateInventoryDetails.getOldProductId(), updateInventoryDetails.getNewProductId());
            return new ResponseEntity<>(updatedInventory, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error updating inventory with oldProductId: {}. Error: {}", updateInventoryDetails.getOldProductId(), e.getMessage(), e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/deleteProductById/{productId}")
    public ResponseEntity<Void> deleteInventory(@PathVariable("productId") Long productId) {
        try {
            logger.debug("Deleting inventory with id: {}", productId);
            inventoryService.deleteInventory(productId);
            logger.info("Successfully deleted inventory with id: {}", productId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            logger.error("Error deleting inventory with id: {}. Error: {}", productId, e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/checkStocks/{productId}")
    public ResponseEntity<Boolean> checkForStock(@PathVariable("productId") Long productId) {
        try {
            logger.debug("Checking stock for productId: {}", productId);
            Boolean isStock = inventoryService.checkStocks(productId);
            logger.info("Stock check result for productId {}: {}", productId, isStock);
            return new ResponseEntity<>(isStock, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Exception occurred while checking stock for productId: {}. Error: {}", productId, e.getMessage(), e);
            return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}