package com.impetus.inventory_service.service;

import com.impetus.inventory_service.model.Inventory;
import com.impetus.inventory_service.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InventoryService {

    @Autowired
    private InventoryRepository inventoryRepository;

    public List<Inventory> getAllInventories() {
        return inventoryRepository.findAll();
    }

    public Optional<Inventory> getInventoryById(Long id) {
        try {
            return Optional.ofNullable(inventoryRepository.findByProductId(id));
        } catch (Exception e) {
            System.out.println("Error getting inventory by id: " + e.getMessage());
            return Optional.empty();
        }
    }

    public Inventory createInventory(Inventory inventory) {
        try {
            return inventoryRepository.save(inventory);
        } catch (Exception e) {
            System.out.println("Error creating inventory: " + e.getMessage());
            return null;
        }
    }

    public Inventory updateInventory(Long id, Inventory inventoryDetails) {
        try {
            Inventory inventory = inventoryRepository.findById(id)
                    .orElseThrow(() -> new Exception("Inventory not found with id " + id));
            inventory.setProductName(inventoryDetails.getProductName());
            inventory.setQuantity(inventoryDetails.getQuantity());
            inventory.setPrice(inventoryDetails.getPrice());
            inventory.setCategory(inventoryDetails.getCategory());
            return inventoryRepository.save(inventory);
        } catch (Exception e) {
            System.out.println("Error updating inventory: " + e.getMessage());
            return null;
        }
    }

    public void deleteInventory(Long id) {
        try {
            Inventory inventory = inventoryRepository.findById(id)
                    .orElseThrow(() -> new Exception("Inventory not found with id " + id));
            inventoryRepository.delete(inventory);
        } catch (Exception e) {
            System.out.println("Error deleting inventory: " + e.getMessage());
        }
    }

    public Boolean checkStocks(Long id) {
        try {
            Inventory inventory = inventoryRepository.findById(id)
                    .orElseThrow(() -> new Exception("Inventory not found with id " + id));
            return inventory.getQuantity() > 0;
        } catch (Exception e) {
            System.out.println("exception occurred {}"+ e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
