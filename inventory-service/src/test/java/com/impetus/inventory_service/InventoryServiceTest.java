package com.impetus.inventory_service;

import com.impetus.inventory_service.model.Inventory;
import com.impetus.inventory_service.repository.InventoryRepository;
import com.impetus.inventory_service.service.InventoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class InventoryServiceTest {

    @InjectMocks
    private InventoryService inventoryService;

    @Mock
    private InventoryRepository inventoryRepository;

    private Inventory inventory;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        inventory = new Inventory(1L, "Product1", 100, 50.0, "Category1");
    }

    @Test
    public void testGetAllInventories() {
        when(inventoryRepository.findAll()).thenReturn(Arrays.asList(inventory));

        var result = inventoryService.getAllInventories();

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Product1", result.get(0).getProductName());
    }

    @Test
    public void testGetInventoryById_Success() {
        when(inventoryRepository.findByProductId(1L)).thenReturn(inventory);

        var result = inventoryService.getInventoryById(1L);

        assertTrue(result.isPresent());
        assertEquals("Product1", result.get().getProductName());
    }

    @Test
    public void testGetInventoryById_Failure() {
        when(inventoryRepository.findByProductId(1L)).thenReturn(null);

        var result = inventoryService.getInventoryById(1L);

        assertFalse(result.isPresent());
    }

    @Test
    public void testCreateInventory_Success() {
        when(inventoryRepository.save(inventory)).thenReturn(inventory);

        var result = inventoryService.createInventory(inventory);

        assertNotNull(result);
        assertEquals("Product1", result.getProductName());
    }

    @Test
    public void testCreateInventory_Failure() {
        when(inventoryRepository.save(inventory)).thenThrow(new RuntimeException("Error"));

        var result = inventoryService.createInventory(inventory);

        assertNull(result);
    }


    @Test
    public void testUpdateInventory_Failure() {
        Inventory updatedInventory = new Inventory(1L, "UpdatedProduct", 150, 60.0, "UpdatedCategory");
        when(inventoryRepository.findById(1L)).thenThrow(new RuntimeException("Error"));

        var result = inventoryService.updateInventory(1L, updatedInventory);

        assertNull(result);
    }

    @Test
    public void testDeleteInventory_Success() throws Exception {
        when(inventoryRepository.findById(1L)).thenReturn(Optional.of(inventory));
        doNothing().when(inventoryRepository).delete(inventory);

        assertDoesNotThrow(() -> inventoryService.deleteInventory(1L));
    }

    @Test
    public void testDeleteInventory_Failure() {
        when(inventoryRepository.findById(1L)).thenThrow(new RuntimeException("Error"));

        assertDoesNotThrow(() -> inventoryService.deleteInventory(1L));
    }

    @Test
    public void testCheckStocks_Available() throws Exception {
        when(inventoryRepository.findById(1L)).thenReturn(Optional.of(inventory));

        var result = inventoryService.checkStocks(1L);

        assertTrue(result);
    }

    @Test
    public void testCheckStocks_NotAvailable() throws Exception {
        Inventory lowStockInventory = new Inventory(1L, "Product1", 0, 50.0, "Category1");
        when(inventoryRepository.findById(1L)).thenReturn(Optional.of(lowStockInventory));

        var result = inventoryService.checkStocks(1L);

        assertFalse(result);
    }

    @Test
    public void testCheckStocks_Exception() {
        when(inventoryRepository.findById(1L)).thenThrow(new RuntimeException("Error"));

        assertThrows(RuntimeException.class, () -> inventoryService.checkStocks(1L));
    }
}