package com.impetus.inventory_service;

import com.impetus.inventory_service.controller.InventoryController;
import com.impetus.inventory_service.dto.ProductUpdateDto;
import com.impetus.inventory_service.model.Inventory;
import com.impetus.inventory_service.service.InventoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class InventoryControllerTest {

    @InjectMocks
    private InventoryController inventoryController;

    @Mock
    private InventoryService inventoryService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(inventoryController).build();
    }

    @Test
    public void testGetAllInventories_Success() throws Exception {
        Inventory inventory1 = new Inventory(1L, "Product1", 10, 100.0, "Category1");
        Inventory inventory2 = new Inventory(2L, "Product2", 20, 200.0, "Category2");
        when(inventoryService.getAllInventories()).thenReturn(Arrays.asList(inventory1, inventory2));

        mockMvc.perform(get("/api/inventory/fetchAllProductByProductId"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].productId").value(1))
                .andExpect(jsonPath("$[0].productName").value("Product1"))
                .andExpect(jsonPath("$[1].productId").value(2))
                .andExpect(jsonPath("$[1].productName").value("Product2"));
    }

    @Test
    public void testGetInventoryById_Success() throws Exception {
        Inventory inventory = new Inventory(1L, "Product1", 10, 100.0, "Category1");
        when(inventoryService.getInventoryById(1L)).thenReturn(Optional.of(inventory));

        mockMvc.perform(get("/api/inventory/fetchProductByProductId/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productId").value(1))
                .andExpect(jsonPath("$.productName").value("Product1"));
    }

    @Test
    public void testGetInventoryById_NotFound() throws Exception {
        when(inventoryService.getInventoryById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(get("/api/inventory/fetchProductByProductId/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateInventory_Success() throws Exception {
        Inventory inventory = new Inventory(1L, "Product1", 10, 100.0, "Category1");
        when(inventoryService.createInventory(any(Inventory.class))).thenReturn(inventory);

        mockMvc.perform(post("/api/inventory/createProduct")
                        .contentType("application/json")
                        .content("{\"productId\":1,\"productName\":\"Product1\",\"quantity\":10,\"price\":100.0,\"category\":\"Category1\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.productId").value(1))
                .andExpect(jsonPath("$.productName").value("Product1"));
    }

    @Test
    public void testUpdateInventory_Success() throws Exception {
        ProductUpdateDto updateDto = new ProductUpdateDto(1L, 2L, "UpdatedProduct", 15, 150.0, "UpdatedCategory");
        Inventory updatedInventory = new Inventory(2L, "UpdatedProduct", 15, 150.0, "UpdatedCategory");
        when(inventoryService.updateInventory(eq(1L), any(Inventory.class))).thenReturn(updatedInventory);

        mockMvc.perform(put("/api/inventory/updateProduct")
                        .contentType("application/json")
                        .content("{\"oldProductId\":1,\"newProductId\":2,\"productName\":\"UpdatedProduct\",\"quantity\":15,\"price\":150.0,\"category\":\"UpdatedCategory\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productId").value(2))
                .andExpect(jsonPath("$.productName").value("UpdatedProduct"));
    }

    @Test
    public void testUpdateInventory_Failure() throws Exception {
        ProductUpdateDto updateDto = new ProductUpdateDto(1L, 2L, "UpdatedProduct", 15, 150.0, "UpdatedCategory");
        when(inventoryService.updateInventory(eq(1L), any(Inventory.class))).thenThrow(new RuntimeException("Update failed"));

        mockMvc.perform(put("/api/inventory/updateProduct")
                        .contentType("application/json")
                        .content("{\"oldProductId\":1,\"newProductId\":2,\"productName\":\"UpdatedProduct\",\"quantity\":15,\"price\":150.0,\"category\":\"UpdatedCategory\"}"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void testDeleteInventory_Success() throws Exception {
        doNothing().when(inventoryService).deleteInventory(1L);

        mockMvc.perform(delete("/api/inventory/deleteProductById/1"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testDeleteInventory_Failure() throws Exception {
        doThrow(new RuntimeException("Deletion failed")).when(inventoryService).deleteInventory(1L);

        mockMvc.perform(delete("/api/inventory/deleteProductById/1"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void testCheckForStock_Success() throws Exception {
        when(inventoryService.checkStocks(1L)).thenReturn(true);

        mockMvc.perform(get("/api/inventory/checkStocks/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(true));
    }

    @Test
    public void testCheckForStock_Failure() throws Exception {
        when(inventoryService.checkStocks(1L)).thenThrow(new RuntimeException("Stock check failed"));

        mockMvc.perform(get("/api/inventory/checkStocks/1"))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$").value(false));
    }
}

