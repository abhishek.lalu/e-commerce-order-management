import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class scratch_3 {


    static class FactorialCalculator implements Callable<Integer> {
        private final int number;

        public FactorialCalculator(int number) {
            this.number = number;
        }

        @Override
        public Integer call() throws Exception {
            int result = 1;
            for (int i = 1; i <= number; i++) {
                result *= i;
                Thread.sleep(100);  // Simulate long computation
            }
            return result;
        }
    }

    public static void main(String[] args) {
        ExecutorService executor = Executors.newSingleThreadExecutor();


        Future<Integer> future = executor.submit(new FactorialCalculator(5));

        try {

            System.out.println("Doing some other tasks while waiting for the result...");
            Integer result = future.get();  // This will block until the result is available

            System.out.println("Factorial of 5 is: " + result);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }
}
