                                                    Hadoop
    HDFS
1.

WHAT IS HDFS 
        => HDFS (Hadoop Distributed File System) is a distributed file system designed to store vast amounts of data reliably, efficiently, and with high throughput. It is the primary storage system used by Hadoop applications. HDFS operates on a master-slave architecture where:

            NameNode: The master node that manages the file system namespace and regulates access to files by clients.
            
			DataNode: Slave nodes that store the actual data blocks and perform read/write operations as instructed by the NameNode and clients.

HDFS USE CASES
        => HDFS is well-suited for applications that have large datasets and require high throughput data access. Some common use cases include:

            Batch Processing: HDFS is used in scenarios where large datasets need to be processed in batches, such as log analysis, data warehousing, and data mining.
			
            Data Warehousing: It can serve as a storage layer for data warehouses, providing scalable and reliable storage for historical data.
			
            Data Lake: HDFS can be used as a central repository to store structured, semi-structured, and unstructured data in its native format, enabling various analytics and processing tasks.
			
            Backup and Archive: HDFS can be used for backup and archival purposes due to its fault tolerance and replication features.

BLOCK STRUCTURE
        => In HDFS, files are divided into large blocks (typically 128 MB or 256 MB), and these blocks are then distributed across the DataNodes in the cluster. The block size is configurable but is typically much larger than the block size in traditional file systems. This large block size helps in reducing the metadata overhead and improves throughput for large-scale data processing.

DATA LOCALITY
       => Data locality is a key principle in HDFS. When a client application reads data from HDFS, the NameNode provides the location information of the required blocks. The client then accesses the data directly from the DataNodes where the blocks are located. This reduces network traffic and improves performance since data is processed locally on the nodes where it resides, minimizing data transfer across the network.


2.

READING DATA FROM HDFS:

       => Using hadoop fs -cat command:
          This command is used to concatenate files and display the output on the console.
          Syntax: hadoop fs -cat /path/to/hdfs/file.txt
          Example: hadoop fs -cat /user/input/file.txt
          It's commonly used to quickly view the contents of a small file stored in HDFS.

       => Using hadoop fs -get command:
          This command is used to copy files or directories from HDFS to the local file system.
          Syntax: hadoop fs -get /path/to/hdfs/source /local/destination
          Example: hadoop fs -get /user/input/file.txt /local/machine/destination
          It's useful when you need to work with files stored in HDFS locally or transfer them to another system.
 
       => Using Hadoop APIs:
	      Hadoop provides APIs in various programming languages like Java, Python, etc., for reading data from HDFS programmatically.
          For example, in Java, you can use the FileSystem class to open an InputStream to read data from a file in HDFS.



WRITING DATA TO HDFS:
       => Using hadoop fs -put command:
          This command is used to copy files or directories from the local file system to HDFS.
          Syntax: hadoop fs -put /local/source /path/to/hdfs/destination
          Example: hadoop fs -put /local/machine/source.txt /user/output/
          It's commonly used to upload data from the local file system to HDFS for processing by Hadoop jobs.
         
	   => Using hadoop fs -copyFromLocal command:
          Similar to hadoop fs -put, this command copies files or directories from the local file system to HDFS.
          Syntax: hadoop fs -copyFromLocal /local/source /path/to/hdfs/destination
          Example: hadoop fs -copyFromLocal /local/machine/source.txt /user/output/
          It serves the same purpose as hadoop fs -put but with a different command syntax.
         
	   => Using Hadoop APIs:
		  Hadoop APIs allow you to write data to HDFS programmatically using various programming languages.
          For example, in Java, you can use the FileSystem class to open an OutputStream to write data to a file on HDFS.
		  

BASIC FILESYSTEM COMMANDS:

       => hadoop fs -ls /path/to/hdfs/directory
       => Basic Filesystem Commands
	   => hadoop fs -rm /path/to/hdfs/file_or_directory
       => hadoop fs -mv /path/to/hdfs/source /path/to/hdfs/destination
       => hadoop fs -du /path/to/hdfs/file_or_directory
       => hadoop fs -chmod <permissions> /path/to/hdfs/file_or_directory
	   

3.	  
    USERS PERMISSIONS IN HADOOP
 
	   Users and Groups:
         - User: An individual interacting with Hadoop, identified by a unique username.
         - Group: A collection of users.

      Permissions:
        - Read (r): Allows reading contents or viewing metadata.
        - Write (w): Allows modifying contents or managing files.
        - Execute (x): Allows executing files or traversing directories.

     Access Control Lists (ACLs):
        - Provide fine-grained control over permissions.
        - Allow granting specific permissions to users and groups.

     Setting Permissions:
        - `hadoop fs -chmod`: Change permissions of files or directories.
        - `hadoop fs -chown`: Change owner and group of files or directories.

     Default Permissions:
        - Administrators can set default permissions for newly created files and directories.

     Security Considerations:
        - Manage permissions to ensure data security.
        - Regularly audit and review permissions for vulnerabilities.
        - Integrate with external authentication systems like LDAP and Kerberos.

4. 
    BLOCKS IN HDFS:

         1. Definition:
             - In HDFS, files are divided into fixed-size blocks (typically 64 MB to 256 MB).
             - Each block is a unit of storage and represents a contiguous portion of a file.

         2. Replication:
             - Each block is replicated across multiple DataNodes (usually three times by default) for fault tolerance.
             - Replication ensures data durability and availability even if some DataNodes fail.

         3. Data Locality:
            - HDFS aims to maximize data locality, meaning computation is performed as close to the data as possible.
            - By storing replicas of blocks on multiple DataNodes, Hadoop reduces network traffic and improves performance by allowing tasks to run where data resides.

         4. Significance of Block Size:
            - Efficient Storage: Larger block sizes reduce metadata overhead and are more efficient for managing large files.
            - Reduced NameNode Load: Fewer blocks per file lessen the burden on the NameNode, enhancing scalability.
            - Optimized Throughput: Larger blocks result in fewer input splits and map tasks, leading to higher throughput.
            - Balancing Factors: Block size choice depends on factors like dataset size, processing needs, and balancing storage efficiency with processing performance.
