class scratch_1 {
    private int count = 0;


    public synchronized void increment() throws InterruptedException {
        count++;
        Thread.sleep(20);
        System.out.println("count = "+count);
    }

    public synchronized int getCount() throws InterruptedException {

        return count;
    }

    public static void main(String[] args) throws InterruptedException {
        scratch_1 counter = new scratch_1();

        
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                try {
                    counter.increment();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                try {
                    counter.increment();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        t1.start();
        t2.start();


        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Print the final count
        System.out.println("Final Count: " + counter.getCount());
    }
}
