                                                      hive notes
													  
	SQL-Like Query Language: 
		Hive provides a language called HiveQL, which is similar to SQL. This allows users familiar with SQL to query the data stored in Hadoop without needing to learn new languages or tools.

	Schema on Read: 
		Unlike traditional databases, which use a schema on write approach, Hive uses schema on read. This means that the schema is applied when data is queried, rather than when it is written.

	Data Storage: 
		Hive can query data stored in various formats, including text files, sequence files, and HBase. It supports file formats like Avro, ORC, Parquet, and more.

	Integration with Hadoop Ecosystem: 
		Hive integrates well with other components of the Hadoop ecosystem, such as HDFS (Hadoop Distributed File System), HBase, Spark, and more.

	Data Partitioning and Bucketing: 
		Hive allows users to partition data based on one or more keys, which can significantly improve query performance. It also supports bucketing, which is a technique for distributing data evenly across a predefined number of files.

	Extensibility: 
		Hive is highly extensible and allows users to write custom functions (UDFs - User Defined Functions) in Java, Python, or other programming languages.

1. EXPLORE HIVE ARCHITECTURE, RUNNING HIVE SQL QUERIES (DML/DDL QUERIES) THROUGH CLI 

	HIVE ARCHITECTURE:
		Hive architecture comprises three main components:
			Metastore: 
				This stores metadata for Hive tables like their schema and location. It does not store the actual data; instead, it stores information about the data, including its format, location, and data types.
			
			HiveQL Processor: 
				This is the component that parses and executes queries. It includes the driver, compiler, and execution engine. The driver takes care of accepting queries, generating execution plans, and submitting these plans to the execution engine.
			
			Hadoop: 
				Hive uses Hadoop for storage and processing. It can run on top of either Hadoop MapReduce or Apache Tez, which are responsible for executing Hive queries.
    
	
	Running Hive SQL Queries through CLI:
	
		DDL (Data Definition Language) Queries:
			-- Create a database
				

			-- Use a database
			USE mydatabase;

			-- Create a table
			CREATE TABLE IF NOT EXISTS mytable (
			  id INT,
			  name STRING,
			  age INT
			);

			-- Describe a table
			DESCRIBE mytable;

			-- Drop a table
			DROP TABLE IF EXISTS mytable;

			-- Drop a database
			DROP DATABASE IF EXISTS mydatabase;
				
		DML (Data Manipulation Language) Queries:
			-- Insert data into a table
			INSERT INTO mytable VALUES (1, 'John', 25), (2, 'Jane', 30);

			-- Select data from a table
			SELECT * FROM mytable;

			-- Update data in a table
			UPDATE mytable SET age = 35 WHERE name = 'Jane';

			-- Delete data from a table
			DELETE FROM mytable WHERE name = 'John';

	To run these queries, you can use the Hive CLI (Command-Line Interface) as follows:
		# Start the Hive CLI
		hive

		# Enter your queries
		CREATE DATABASE IF NOT EXISTS mydatabase;
		USE mydatabase;
		CREATE TABLE IF NOT EXISTS mytable (
		  id INT,
		  name STRING,
		  age INT
		);
		-- Insert some data
		INSERT INTO mytable VALUES (1, 'John', 25), (2, 'Jane', 30);
		-- Select data
		SELECT * FROM mytable;

		# To exit the Hive CLI, type
		quit;
		
		
		
		
2. HIVE INTEGRATION WITH HDFS (EXPORTING/IMPORTING DATA TO/FROM HIVE TABLES TO HDFS)

	Hive Integration with HDFS

	Hive integrates seamlessly with Hadoop Distributed File System (HDFS) for data storage and retrieval. Here's how you can export/import data between Hive tables and HDFS:

	EXPORTING DATA FROM A HIVE TABLE TO HDFS:
		-- Export data from a Hive table to HDFS
		INSERT OVERWRITE DIRECTORY '/user/hive/output'
		SELECT * FROM mytable;
		
	IMPORTING DATA FROM HDFS TO A HIVE TABLE:
		-- Import data from HDFS to a Hive table
		LOAD DATA INPATH '/user/hive/input' INTO TABLE mytable;

	TO EXECUTE THESE QUERIES, YOU CAN USE THE HIVE CLI:
		# Start the Hive CLI
		hive

		# Export data from a Hive table to HDFS
		INSERT OVERWRITE DIRECTORY '/user/hive/output'
		SELECT * FROM mytable;

		# Import data from HDFS to a Hive table
		LOAD DATA INPATH '/user/hive/input' INTO TABLE mytable;

		# Exit the Hive CLI
		quit;
		

3. WHAT IS BEELINE AND CONNECT IT WITH HIVE SERVER 2 TO RUN QUERIES.
	
	Beeline and Connecting to Hive Server 2
		Beeline is a command-line tool provided by Apache Hive that allows you to interact with HiveServer2. Here's how you can connect to HiveServer2 and run queries using Beeline:
			Connect to HiveServer2 using Beeline:
				beeline -u jdbc:hive2://<HiveServer2_IP>:<Port>/<database_name> -n <username> -p <password>
			Example:
				beeline -u jdbc:hive2://localhost:10000/default -n myuser -p mypassword
			Once connected, you can run SQL queries using Beeline:
				-- Create a database
				CREATE DATABASE IF NOT EXISTS mydatabase;

				-- Use a database
				USE mydatabase;

				-- Create a table
				CREATE TABLE IF NOT EXISTS mytable (
				  id INT,
				  name STRING,
				  age INT
				);

				-- Insert data into a table
				INSERT INTO mytable VALUES (1, 'John', 25), (2, 'Jane', 30);

				-- Select data from a table
				SELECT * FROM mytable;

				-- Exit Beeline
				!quit;

DATA TYPES IN HIVE
	CREATE TABLE my_table (
	  tinyint_col TINYINT,
	  smallint_col SMALLINT,
	  int_col INT,
	  bigint_col BIGINT,
	  boolean_col BOOLEAN,
	  float_col FLOAT,
	  double_col DOUBLE,
	  string_col STRING,
	  char_col CHAR(10),
	  varchar_col VARCHAR(255),
	  decimal_col DECIMAL(10, 2),
	  date_col DATE,
	  timestamp_col TIMESTAMP,
	  array_col ARRAY<INT>,
	  map_col MAP<STRING, INT>,
	  struct_col STRUCT<name: STRING, age: INT>,
	  union_col UNIONTYPE<INT, FLOAT, STRING>
	);

4. MAP SIDE JOIN

		Map-side join is a technique used in distributed data processing frameworks like Apache Spark to perform joins more efficiently by minimizing data shuffling. In a map-side join, one of the datasets involved in the join operation is small enough to fit entirely in memory on each worker node. Instead of shuffling the entire dataset across the cluster, the smaller dataset is broadcasted to all worker nodes, and the join is performed locally on each node.

		However, map-side join is not applicable when both datasets are large or when the join condition cannot be satisfied by broadcasting one of the datasets due to its size.

5.STRICT MODE AND ITS ADVANTAGES.
    
	Strict mode in Hive enforces rules that prevent potentially expensive or harmful queries from being executed. It helps users avoid running queries that could lead to performance issues or inefficient data processing. Strict mode is controlled by the configuration parameter hive.mapred.mode.
	
	There are three possible values for hive.mapred.mode:
		nonstrict (default): No restrictions are applied.
		strict: Restrictions are applied to certain types of queries.
		nostrict: This is an alias for nonstrict and has the same effect.
	
	Key Restrictions in Strict Mode
		Disallow Full Table Scans:Hive will restrict queries that perform full table scans on partitioned tables unless a partition filter is specified.
		This helps in avoiding large-scale scans which can be very expensive and time-consuming.

		Require Partition Filters:Queries on partitioned tables must include a partition filter, ensuring that only a subset of the data is processed.

		Disallow Cartesian Products:Joins that could result in Cartesian products (cross joins) are not allowed, which helps in preventing accidental generation of extremely large result sets.
		Disallow ORDER BY without LIMIT:Hive enforces that queries with an ORDER BY clause must also include a LIMIT clause to avoid sorting large datasets entirely in memory, which can cause performance degradation.
	
	## Advantages of Strict Mode

		**Performance Optimization**:
		- By enforcing partition filters and limiting full table scans, strict mode ensures that only relevant data is processed, leading to faster query execution and reduced resource usage.

		**Resource Management**:
		- Preventing full table scans and large result sets helps in better management of cluster resources, avoiding scenarios where a single query can monopolize the cluster resources.

		**Query Safety**:
		- Enforcing restrictions like disallowing Cartesian products helps in preventing accidental execution of queries that could generate enormous result sets, potentially crashing the cluster.

		**Encourages Best Practices**:
		- Strict mode nudges users towards writing more efficient and optimized queries, promoting best practices in data handling and processing.

		## Enabling Strict Mode

		To enable strict mode in Hive, you need to set the `hive.mapred.mode` parameter to `strict`. This can be done in the Hive configuration file (hive-site.xml) or dynamically using the Hive command line or a script.

	### Setting in hive-site.xml

		```xml
		<property>
		  <name>hive.mapred.mode</name>
		  <value>strict</value>
		  <description>Set Hive to strict mode to disallow full table scans and other potentially expensive operations.</description>
		</property>

	example:-
			%sql``
			--this is to illustrate strict mode in HIVE 
			-- Enable STRICT mode
			SET hive.exec.dynamic.partition.mode=STRICT;

			-- Create a table with strict schema
			CREATE TABLE IF NOT EXISTS employee (
				id INT,
				name STRING,
				age INT,
				department STRING
			) STORED AS ORC;


6.WHAT IS  RUNNING HIVE QUERY IN LOCAL MODE.
	Running Hive queries in local mode refers to executing Hive queries using the local resources of a single machine, rather than leveraging a distributed cluster environment such as Hadoop YARN. This can be useful for development, testing, and debugging purposes when working with small datasets or when cluster resources are not available.	

	## Advantages of Running Hive in Local Mode

		**Simplified Setup**:
		- No need for a full Hadoop or YARN cluster setup.
		- Easier to configure and manage for small-scale operations.

		**Faster Iteration**:
		- Ideal for development and testing as it allows for quick iteration without waiting for cluster resource allocation.

		**Cost-Effective**:
		- Reduces costs by not requiring cluster resources, making it suitable for lightweight tasks and development environments.

		**Accessibility**:
		- Useful for environments with limited infrastructure, providing access to Hive’s features without needing a full-scale Hadoop environment.


7.PARTITIONING, DYNAMIC PARTITIONING IN HIVE
	Partitioning in Hive is a technique used to organize data stored in tables into multiple directories based on the values of one or more columns. This improves query performance by allowing Hive to skip scanning unnecessary data when filtering on the partitioned columns.
	
	Advantages of Partitioning:
		
		Improved Query Performance: Queries that filter on partitioned columns can be executed more efficiently because Hive only needs to scan the relevant partitions.
		
		Data Organization:Data is logically organized into directories based on partition keys, making it easier to manage and analyze.
		
		Reduced Disk I/O:By reading only the necessary partitions, Hive reduces disk I/O and improves overall query performance.
	
	Example:
			CREATE TABLE employee (
			  id INT,
			  name STRING,
			  age INT
			)
			PARTITIONED BY (department STRING, country STRING);


    DYNAMIC PARTITIONING IN HIVE:
		Dynamic partitioning in Hive allows the system to automatically determine the partition values based on the data being inserted. This is particularly useful when dealing with large datasets where manually specifying partition values for each insert operation is impractical.
		
		Advantages of Dynamic Partitioning:
			
			Automated Partitioning:Eliminates the need to manually specify partition values for each insert operation, reducing human error and improving efficiency.
			
			Flexibility:Allows partitions to be created dynamically based on the actual data, accommodating changes in data distribution over time.
			
			Simplifies Data Loading:Streamlines the data loading process, especially for large datasets, by automatically determining partition values.
		
		Example:
			SET hive.exec.dynamic.partition = true;
			SET hive.exec.dynamic.partition.mode = nonstrict;

			INSERT OVERWRITE TABLE employee PARTITION (department, country)
			SELECT id, name, age, department, country FROM employee_data;
