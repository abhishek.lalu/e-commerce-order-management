package com.example.ecommerce.notificationManagementService.service;

import com.example.ecommerce.notificationManagementService.dto.NotificationDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class HandleOrderEvents {

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private ObjectMapper objectMapper;

    private static final Logger logger = LoggerFactory.getLogger(HandleOrderEvents.class);

    @KafkaListener(topics = "orderNotification", groupId = "notificationSend")
    public void handleEvents(String notification) throws JsonProcessingException {

        NotificationDto notificationDto = objectMapper.readValue(notification, NotificationDto.class);
        logger.info(String.format("inside handlerOrderEvents to handle notification message --> %s ", notification));
        notificationService.sendNotification(notificationDto);

    }



}
