package com.example.ecommerce.notificationManagementService.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDetailsDto {

    private String firstName;

    private String lastName;

    public UserDetailsDto() {
    }

    public UserDetailsDto(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
