package com.example.ecommerce.notificationManagementService.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserIdDto {

    private Long userId;

    public UserIdDto() {
    }

    public UserIdDto(Long userId) {
        this.userId = userId;
    }
}
