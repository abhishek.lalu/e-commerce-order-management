package com.example.ecommerce.notificationManagementService.service;

import com.example.ecommerce.notificationManagementService.dto.NotificationDto;
import com.example.ecommerce.notificationManagementService.dto.UserIdDto;
import com.example.ecommerce.notificationManagementService.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class NotificationService {

    @Autowired
    private RestTemplate restTemplate;

    public String sendNotification(NotificationDto notificationDto) {

        String message = notificationDto.getMessage();

//        String productName = notificationDto.getProductName();

        Long userId = notificationDto.getUserId();

        String userName = getUserNameFromUserId(userId);

        String result;
        result = "Dear " + userName + ",\n" + message;

        return result;

    }

    private String getUserNameFromUserId(Long userId) {


        ResponseEntity<UserDetailsDto> responseEntity = restTemplate.getForEntity("http://localhost:8085/user/userDetails/" + userId, UserDetailsDto.class);

        UserDetailsDto dto = new UserDetailsDto();

        dto = responseEntity.getBody();

        if (dto != null) {
            String firstName = dto.getFirstName();

            String lastName = dto.getLastName();

            return firstName + " " + lastName;

        }
        else{
            return null;
        }
    }


}
