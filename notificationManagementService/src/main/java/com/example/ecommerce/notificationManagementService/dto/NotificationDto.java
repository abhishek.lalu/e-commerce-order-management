package com.example.ecommerce.notificationManagementService.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationDto {

    private String productName;

    private String message;

    private Long userId;

    public NotificationDto() {
    }

    public NotificationDto(String productName, String message, Long userId) {
        this.productName = productName;
        this.message = message;
        this.userId = userId;
    }
}
