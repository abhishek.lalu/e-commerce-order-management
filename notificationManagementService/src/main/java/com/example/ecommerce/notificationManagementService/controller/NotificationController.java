package com.example.ecommerce.notificationManagementService.controller;

import com.example.ecommerce.notificationManagementService.dto.NotificationDto;
import com.example.ecommerce.notificationManagementService.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/notification")
public class NotificationController {

    @Autowired
    NotificationService notificationService;

    @PostMapping("/send")
    public ResponseEntity<?> sendNotification(@RequestBody NotificationDto notificationDto){

        String notification = notificationService.sendNotification(notificationDto);

        return new ResponseEntity<>(notification, HttpStatus.OK);
    }

}
