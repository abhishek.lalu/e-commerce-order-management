package com.impetus.payment_service;


import com.impetus.payment_service.model.Payment;
import com.impetus.payment_service.repository.PaymentRepository;
import com.impetus.payment_service.service.PaymentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class PaymentServiceTest {

    @InjectMocks
    private PaymentService paymentService;

    @Mock
    private PaymentRepository paymentRepository;

    private Payment payment;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        payment = new Payment(1L, 101L, LocalDate.now(), 500.0, "Pending", "CreditCard");
    }

    @Test
    void testProcessPayment_Success() {
        when(paymentRepository.save(any(Payment.class))).thenReturn(payment);
        Payment processedPayment = paymentService.processPayment(payment);
        assertNotNull(processedPayment);
        assertEquals("Processed", processedPayment.getStatus());
        verify(paymentRepository, times(1)).save(payment);
    }

    @Test
    void testProcessPayment_Failure() {
        when(paymentRepository.save(any(Payment.class))).thenThrow(new RuntimeException("Database error"));
        Payment processedPayment = paymentService.processPayment(payment);
        assertNull(processedPayment);
        verify(paymentRepository, times(1)).save(payment);
    }

    @Test
    void testRefundPayment_Success() throws Exception {
        when(paymentRepository.findById(1L)).thenReturn(Optional.of(payment));
        when(paymentRepository.save(any(Payment.class))).thenReturn(payment);
        Payment refundedPayment = paymentService.refundPayment(1L);
        assertNotNull(refundedPayment);
        assertEquals("Refunded", refundedPayment.getStatus());
        verify(paymentRepository, times(1)).findById(1L);
        verify(paymentRepository, times(1)).save(payment);
    }

    @Test
    void testRefundPayment_PaymentNotFound() {
        when(paymentRepository.findById(1L)).thenReturn(Optional.empty());
        Payment refundedPayment = paymentService.refundPayment(1L);
        assertNull(refundedPayment);
        verify(paymentRepository, times(1)).findById(1L);
        verify(paymentRepository, never()).save(any(Payment.class));
    }

    @Test
    void testGetPaymentStatus_Success() {
        when(paymentRepository.findById(1L)).thenReturn(Optional.of(payment));
        Optional<Payment> result = paymentService.getPaymentStatus(1L);
        assertTrue(result.isPresent());
        assertEquals(1L, result.get().getPaymentId());
        verify(paymentRepository, times(1)).findById(1L);
    }

    @Test
    void testGetPaymentStatus_PaymentNotFound() {
        when(paymentRepository.findById(1L)).thenReturn(Optional.empty());
        Optional<Payment> result = paymentService.getPaymentStatus(1L);
        assertFalse(result.isPresent());
        verify(paymentRepository, times(1)).findById(1L);
    }
}
