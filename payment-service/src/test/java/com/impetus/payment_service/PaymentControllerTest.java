package com.impetus.payment_service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.impetus.payment_service.controller.PaymentController;
import com.impetus.payment_service.model.Payment;
import com.impetus.payment_service.service.PaymentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(PaymentController.class)
public class PaymentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PaymentService paymentService;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void testProcessPayment() throws Exception {
        Payment payment = new Payment();
        payment.setPaymentId(1L);
        payment.setAmount(100.0);

        when(paymentService.processPayment(any(Payment.class))).thenReturn(payment);

        mockMvc.perform(post("/api/payment/process")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(payment)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.paymentId").value(1))
                .andExpect(jsonPath("$.amount").value(100.0));
    }

    @Test
    public void testRefundPayment() throws Exception {
        Payment payment = new Payment();
        payment.setPaymentId(1L);

        Payment refundedPayment = new Payment();
        refundedPayment.setPaymentId(1L);
        refundedPayment.setAmount(100.0);

        when(paymentService.refundPayment(anyLong())).thenReturn(refundedPayment);

        mockMvc.perform(post("/api/payment/refund")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(payment)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.paymentId").value(1))
                .andExpect(jsonPath("$.amount").value(100.0));
    }

    @Test
    public void testGetPaymentStatus() throws Exception {
        Payment payment = new Payment();
        payment.setPaymentId(1L);
        payment.setAmount(100.0);

        when(paymentService.getPaymentStatus(anyLong())).thenReturn(java.util.Optional.of(payment));

        mockMvc.perform(get("/api/payment/status/getPaymentStatus/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.paymentId").value(1))
                .andExpect(jsonPath("$.amount").value(100.0));
    }

    @Test
    public void testGetPaymentStatusNotFound() throws Exception {
        when(paymentService.getPaymentStatus(anyLong())).thenReturn(java.util.Optional.empty());

        mockMvc.perform(get("/api/payment/status/getPaymentStatus/1"))
                .andExpect(status().isNotFound());
    }
}
