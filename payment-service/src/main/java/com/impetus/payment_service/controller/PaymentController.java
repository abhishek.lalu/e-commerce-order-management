package com.impetus.payment_service.controller;

import com.impetus.payment_service.model.Payment;
import com.impetus.payment_service.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {

    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    private PaymentService paymentService;

    @PostMapping("/process")
    public ResponseEntity<Payment> processPayment(@RequestBody Payment payment) {
        logger.info("Processing payment request: {}", payment);
        try {
            Payment processedPayment = paymentService.processPayment(payment);
            logger.info("Payment processed successfully: {}", processedPayment);
            return new ResponseEntity<>(processedPayment, HttpStatus.CREATED);
        }
        catch (Exception e) {
            logger.error("Error processing payment for {}: ", payment, e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/refund")
    public ResponseEntity<Payment> refundPayment(@RequestBody Payment payment) {
        logger.info("Refunding payment with ID: {}", payment.getPaymentId());
        try {
            Payment refundedPayment = paymentService.refundPayment(payment.getPaymentId());
            logger.info("Payment refunded successfully: {}", refundedPayment);
            return new ResponseEntity<>(refundedPayment, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error refunding payment with ID {}: ", payment.getPaymentId(), e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/status/getPaymentStatus/{paymentId}")
    public ResponseEntity<Payment> getPaymentStatus(@PathVariable("paymentId") Long paymentId) {
        logger.info("Fetching payment status for ID: {}", paymentId);
        try {
            Payment payment = paymentService.getPaymentStatus(paymentId)
                    .orElseThrow(() -> new Exception("Payment not found with ID " + paymentId));
            logger.info("Payment status retrieved successfully: {}", payment);
            return new ResponseEntity<>(payment, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error getting payment status for ID {}: ", paymentId, e);
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
