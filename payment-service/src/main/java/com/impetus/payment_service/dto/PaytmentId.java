package com.impetus.payment_service.dto;

public class PaytmentId {
    private Long paytmentID;

    public PaytmentId() {
    }

    public PaytmentId(Long paytmentID) {
        this.paytmentID = paytmentID;
    }

    public Long getPaytmentID() {
        return paytmentID;
    }

    public void setPaytmentID(Long paytmentID) {
        this.paytmentID = paytmentID;
    }
}
