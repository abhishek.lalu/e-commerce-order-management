package com.impetus.payment_service.service;

import com.impetus.payment_service.model.Payment;
import com.impetus.payment_service.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    public Payment processPayment(Payment payment) {
        try {
            payment.setStatus("Processed");
            return paymentRepository.save(payment);
        } catch (Exception e) {
            System.out.println("Error processing payment: " + e.getMessage());
            return null;
        }
    }

    public Payment refundPayment(Long paymentId) {
        try {
            Payment payment = paymentRepository.findById(paymentId)
                    .orElseThrow(() -> new Exception("Payment not found with id " + paymentId));
            payment.setStatus("Refunded");
            return paymentRepository.save(payment);
        } catch (Exception e) {
            System.out.println("Error refunding payment: " + e.getMessage());
            return null;
        }
    }

    public Optional<Payment> getPaymentStatus(Long paymentId) {
        try {
            return paymentRepository.findById(paymentId);
        } catch (Exception e) {
            System.out.println("Error getting payment status: " + e.getMessage());
            return  Optional.empty();
        }
    }
}