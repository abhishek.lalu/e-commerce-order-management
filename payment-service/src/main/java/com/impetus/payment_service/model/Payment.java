package com.impetus.payment_service.model;


import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Date;

@Setter
@Getter
@Entity


public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long paymentId;
    private Long orderId;
    private LocalDate paymentDate;
    private double amount;
    private String status;
    private String paymentMethod;

    public Payment() {
    }

    public Payment(Long paymentId, Long orderId, LocalDate paymentDate, double amount, String status, String paymentMethod) {
        this.paymentId = paymentId;
        this.orderId = orderId;
        this.paymentDate = paymentDate;
        this.amount = amount;
        this.status = status;
        this.paymentMethod = paymentMethod;
    }

}
