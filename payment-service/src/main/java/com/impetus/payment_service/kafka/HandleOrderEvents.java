package com.impetus.payment_service.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.impetus.payment_service.model.Payment;
import com.impetus.payment_service.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class HandleOrderEvents {
    @Autowired
    PaymentService paymentService;

    @Autowired
    ObjectMapper objectMapper;
    private static final Logger logger = LoggerFactory.getLogger(HandleOrderEvents.class);
    @KafkaListener(topics = "orderPayment", groupId = "paymentProcess")
    public void handleOrderEvents(String payment) throws JsonProcessingException {
        Payment paymentJson  = objectMapper.readValue(payment, Payment.class);
        logger.info(String.format("inside handleOrderEvents to handle message--> %s ",payment));
        paymentService.processPayment(paymentJson);
    }

}
