package com.example.ecommerce.orderManagementService.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "Status")
@Getter
@Setter
public class Status {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long statusId;

    @Column(name = "orderStatus", nullable = false, length = 25)
    private String orderStatus;

    public Status() {
    }

    public Status(Long id, String orderStatus) {
        this.statusId = id;
        this.orderStatus = orderStatus;
    }

    @Override
    public String toString() {
        return "Status{" +
                "id=" + statusId +
                ", orderStatus='" + orderStatus + '\'' +
                '}';
    }

}
