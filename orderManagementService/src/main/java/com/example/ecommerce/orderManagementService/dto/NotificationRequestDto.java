package com.example.ecommerce.orderManagementService.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationRequestDto {

    private String message;

    private Long userId;

    public NotificationRequestDto() {
    }

    public NotificationRequestDto(String message, Long userId) {
        this.message = message;
        this.userId = userId;
    }
}
