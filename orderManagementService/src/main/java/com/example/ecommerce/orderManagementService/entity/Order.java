package com.example.ecommerce.orderManagementService.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "Orders")
@Getter
@Setter
public class Order {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
    private Long orderId;

    @Column(name = "productId", nullable = false)
    private Long productId;

    @Column(name = "userId", nullable = false)
    private Long userId;

    @Column(name = "orderDate", nullable = false)
    @JsonIgnore
    private LocalDate orderDate;

    @Column(name = "deliveryDate", nullable = false)
    @JsonIgnore
    private LocalDate deliveryDate;

    @Column(name = "deliveryAddress", nullable = false)
    private String deliveryAddress;

    @Column(name = "totalCost", nullable = false)
    @JsonIgnore
    private Double totalCost;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Status status;

    @Column(name = "paymentMethod")
    private String paymentMethod;

    @Column(name = "paymentDate")
    private LocalDate paymentDate;

    public Order() {
    }

    public Order(Long orderId, Long productId, Long userId, LocalDate orderDate, LocalDate deliveryDate, String deliveryAddress, Double totalCost, Integer quantity, Status status, String paymentType, LocalDate paymentDate) {
        this.orderId = orderId;
        this.productId = productId;
        this.userId = userId;
        this.orderDate = orderDate;
        this.deliveryDate = deliveryDate;
        this.deliveryAddress = deliveryAddress;
        this.totalCost = totalCost;
        this.quantity = quantity;
        this.status = status;
        this.paymentMethod = paymentType;
        this.paymentDate = paymentDate;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", productId=" + productId +
                ", userId=" + userId +
                ", orderDate=" + orderDate +
                ", deliveryDate=" + deliveryDate +
                ", deliveryAddress='" + deliveryAddress + '\'' +
                ", totalCost=" + totalCost +
                ", quantity=" + quantity +
                ", status=" + status +
                ", paymentType='" + paymentMethod + '\'' +
                ", paymentDate=" + paymentDate +
                '}';
    }
}
