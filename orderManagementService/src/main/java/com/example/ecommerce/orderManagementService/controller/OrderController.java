package com.example.ecommerce.orderManagementService.controller;

import com.example.ecommerce.orderManagementService.dto.OrderResponse;
import com.example.ecommerce.orderManagementService.dto.StatusDto;
import com.example.ecommerce.orderManagementService.entity.Order;
import com.example.ecommerce.orderManagementService.service.OrderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/create")
    public ResponseEntity<?> createOrder(@RequestBody Order order) throws JsonProcessingException {

        String result = orderService.createOrder(order);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @PostMapping("/updateDeliveryDate/{orderId}")
    public ResponseEntity<?> updateDeliveryDate(@PathVariable("orderId") Long orderId) throws JsonProcessingException {

        String result = orderService.updateDeliveryDate(orderId);

        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @PostMapping("/changeStatus/{orderId}")
    public ResponseEntity<?> changeOrderStatus(@PathVariable("orderId") Long orderId, @RequestBody StatusDto statusDto) throws JsonProcessingException {

        String result = orderService.changeOrderStatus(orderId, statusDto);

        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @GetMapping("/fetch/{orderId}")
    public ResponseEntity<?> fetchOrderDetailsById(@PathVariable("orderId") Long orderId){
        Order result = orderService.fetchOrderById(orderId);

        OrderResponse response = new OrderResponse();

        response.setDeliveryAddress(result.getDeliveryAddress());
        response.setQuantity(result.getQuantity());
        response.setOrderStatus(result.getStatus().getOrderStatus());
        response.setPaymentMethod(result.getPaymentMethod());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
