package com.example.ecommerce.orderManagementService.dto;

import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatusDto {

    private Long id;

    private String orderStatus;


    public StatusDto() {
    }

    public StatusDto(Long id, String orderStatus) {
        this.id = id;
        this.orderStatus = orderStatus;
    }
}
