package com.example.ecommerce.orderManagementService.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentStatusDto {

    private String status;

    public PaymentStatusDto() {
    }

    public PaymentStatusDto(String status) {
        this.status = status;
    }
}
