package com.example.ecommerce.orderManagementService.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
public class PaymentDto {

    private Long orderId;

    private LocalDate paymentDate;

    private Double amount;

    private String paymentMethod;

    private String status;

    public PaymentDto() {
    }

    public PaymentDto(Long orderId, LocalDate paymentDate, double amount, String paymentMethod, String status) {
        this.orderId = orderId;
        this.paymentDate = paymentDate;
        this.amount = amount;
        this.paymentMethod = paymentMethod;
        this.status = status;
    }

    public PaymentDto(Long orderId, LocalDate paymentDate, Double amount, String paymentMethod) {
        this.orderId = orderId;
        this.paymentDate = paymentDate;
        this.amount = amount;
        this.paymentMethod = paymentMethod;
    }
}
