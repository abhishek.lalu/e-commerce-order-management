package com.example.ecommerce.orderManagementService.repository;

import com.example.ecommerce.orderManagementService.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

   // public void delete(Optional<Order> optionalOrder);
}
