package com.example.ecommerce.orderManagementService.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderResponse {

    private String deliveryAddress;

    private Integer quantity;

    private String orderStatus;

    private String paymentMethod;

    public OrderResponse() {
    }

    public OrderResponse(String deliveryAddress, Integer quantity, String orderStatus, String paymentMethod) {
        this.deliveryAddress = deliveryAddress;
        this.quantity = quantity;
        this.orderStatus = orderStatus;
        this.paymentMethod = paymentMethod;
    }
}