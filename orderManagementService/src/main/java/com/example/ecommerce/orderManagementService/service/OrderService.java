package com.example.ecommerce.orderManagementService.service;

import com.example.ecommerce.orderManagementService.dto.*;
import com.example.ecommerce.orderManagementService.entity.Order;
import com.example.ecommerce.orderManagementService.entity.Status;
import com.example.ecommerce.orderManagementService.repository.OrderRepository;
import com.example.ecommerce.orderManagementService.repository.StatusRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private RestTemplate restTemplate;
    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    ObjectMapper objectMapper;


    public String createOrder(Order order) throws JsonProcessingException {

        Order orderItem = new Order();

        Double finalCost = order.getQuantity() * 250D;

        Status status = statusRepository.findById(order.getStatus().getStatusId()).orElseThrow(() -> new RuntimeException("Status not found"));

        LocalDate delivery = LocalDate.now().plusDays(7);

        String productName = getProductNameFromProductId(order.getProductId());

        String message = "Your order has been created for " + productName + ".\n" + "Thank you for shopping with us.";

        Double value = Math.random();

        double doubleValue = value * 900;

        Long id = (long)doubleValue;

        orderItem.setOrderId(id);
        orderItem.setProductId(order.getProductId());
        orderItem.setUserId(order.getUserId());
        orderItem.setOrderDate(LocalDate.now());
        orderItem.setDeliveryDate(delivery);
        orderItem.setQuantity(order.getQuantity());
        orderItem.setStatus(status);
        orderItem.setDeliveryAddress(order.getDeliveryAddress());
        orderItem.setTotalCost(finalCost);
        orderItem.setPaymentMethod(order.getPaymentMethod());

        if(order.getPaymentMethod().equals("Cash on delivery")){
            orderItem.setPaymentDate(delivery);
        }
        else {
            orderItem.setPaymentDate(LocalDate.now());
        }

        PaymentStatusDto paymentDto = proceedToPayment(orderItem);

        if(paymentDto.getStatus().equals("Processed")){
            orderRepository.save(orderItem);
            String notification = sendNotification(order, message);
            return notification;
        }
        else {
            return "Facing problem with payment";
        }

    }

    public String updateDeliveryDate(Long orderId) throws JsonProcessingException {

        Order order = orderRepository.findById(orderId).orElseThrow(()->new RuntimeException("order not found"));

        LocalDate updatedDeliveryDate = order.getDeliveryDate().plusDays(2);

        String productName = getProductNameFromProductId(order.getProductId());

        String message = "Delivery date has been updated for " + productName + ".\n" + "Thank you for shopping with us.";

        order.setDeliveryDate(updatedDeliveryDate);

        orderRepository.save(order);

        String notification = sendNotification(order, message);

        return notification;

    }

    public String changeOrderStatus(Long orderId, StatusDto statusDto) throws JsonProcessingException {

        Order order = orderRepository.findById(orderId).orElseThrow(()-> new RuntimeException("order not found"));

        Status status = statusRepository.findById(statusDto.getId()).orElseThrow(() -> new RuntimeException(("not found")));

        String productName = getProductNameFromProductId(order.getProductId());

        String message = "Your order has been "+ status.getOrderStatus() + " for " + productName + ".\n" + "Thank you for shopping with us.";

        order.setStatus(status);

        orderRepository.save(order);

        String notification = sendNotification(order, message);

        return notification;

    }

    private String sendNotification(Order order, String message) throws JsonProcessingException {

        NotificationRequestDto notification = new NotificationRequestDto();

        String url = "http://localhost:8084/notification/send";

        NotificationRequestDto notificationRequestDto = new NotificationRequestDto(message, order.getUserId());

        String result;
        result = restTemplate.postForObject(url, notificationRequestDto, String.class);
        String notificationString = objectMapper.writeValueAsString(notificationRequestDto);
        kafkaTemplate.send("orderNotification",notificationString);
        logger.info(String.format("message sent to kafka topic orderNotification {} %s",notificationString));

        return result;

    }

    private String getProductNameFromProductId(Long productId){

        ResponseEntity<InventoryDto> responseEntity = restTemplate.getForEntity("http://localhost:8083/api/inventory/fetchProductByProductId/"  + productId, InventoryDto.class );

        InventoryDto inventoryDto = responseEntity.getBody();

        String productName;
        productName = inventoryDto.getProductName();

        return productName;

    }

    private PaymentStatusDto proceedToPayment(Order order) throws JsonProcessingException {

        PaymentDto payment = new PaymentDto(order.getOrderId(), order.getPaymentDate(), order.getTotalCost(), order.getPaymentMethod(),order.getStatus().getOrderStatus());
        String paymentString = objectMapper.writeValueAsString(payment);
        kafkaTemplate.send("orderPayment",paymentString);
        logger.info(String.format("message sent to kafka topic orderPayment {} %s",paymentString));
        PaymentDto dto = restTemplate.postForObject("http://localhost:8082/api/payment/process",payment, PaymentDto.class);

        PaymentStatusDto status = new PaymentStatusDto();
        assert dto != null;
        status.setStatus(dto.getStatus());

        return status;

    }

    public Order fetchOrderById(Long orderId) {
        return orderRepository.findById(orderId).orElseThrow(() -> new RuntimeException("Order not found"));
    }


}
