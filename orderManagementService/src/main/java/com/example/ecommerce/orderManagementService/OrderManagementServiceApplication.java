package com.example.ecommerce.orderManagementService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class OrderManagementServiceApplication {

	public static void main(String[] args) {

		SpringApplication.run(OrderManagementServiceApplication.class, args);
	}

}
