package com.example.ecommerce.orderManagementService.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InventoryDto {

    private String productName;

    public InventoryDto() {
    }

    public InventoryDto(String productName) {
        this.productName = productName;
    }
}
